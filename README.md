# UnityTAF4IA

## About
This repository contains all homework done for the module "Unity" of 4th AI 2022-2023.

Each of the following tasks' source code is found in corresponding branch.

## First task : Solar system
A recreation of the solar systeme where the moon turns around earth that is turning around the sun. 

![solarSystemDemo](/uploads/1c4feace2c451daccb679d815dc472ff/My_Video.mp4)

## Second task : Square mouvement
A sphere that is doing a square-shaped loop mouvement.

![mouvementCarreDemo](/uploads/5c6eff7c184f65066f0f05fc8dae4975/mouvementCarreDemo.mp4)

## Third task : PacMan
A basic PacMan game.

![PacManDemo](/uploads/5ded8b23cf1b582958dc987505eac668/My_Video1.mp4)

## Fourth task : Subway Surfers
A simple replica of subway surfers.

![subwayV1Demo](/uploads/cc83c1e9cb6c212d7e84c812203d5b0e/subwayV1.mp4)

## Fith task : PacManv2
Updated PacMan : changed grafics, generating 3 ennemies with random positions, when touched by ennemy game over and player is able to restart the game with a specific screen.

![pacmanV2demo](/uploads/a42ee8aad12e23d732b86379d9a30849/pacmanV2demo.mp4)

## Fourth task : Subway Surfers
Updated SubwaySurfers : endless run, random and automatic obstacles' generation.

![subwayV2Demo](/uploads/07c0dce8a2495606d1b0001a9e8ef811/subwayV2.mp4)

## Fifth task : PacManv3
Updated pacman : for this version we disabled the following of the player by the ennemies ( will correct later ), added lose and win UI.

![pacmanV3demo](/uploads/f942339875385edd69ebcaee39e9f4fe/pacmanV3demo.mp4)

## Sixth task : Super Mario 2D
Mario Game with ennemies .

![Mario2Ddemo](/uploads/3e52bbca51239acc26540755eb5487f5/My_Video.mp4)



## Credits : 
This work is done by :


| Semah Kadri | Mohamed Aziz Gallala |
| ------ | ------ |
|  ![semah](/uploads/aab717c73dfd964abcbabae6ca6b6335/semah.jpg)      |   ![minelower](/uploads/164815d65ab9e0d2ce3a14aaeb4d108f/minelower.jpg)     |

